﻿using UnityEngine;
using System.Collections;

public abstract class Character : MonoBehaviour {
	


	[SerializeField]
	protected float MovementSpeed;

	protected bool facingRight;

	public bool Attack { get; set; }

	public Animator MyAnimator { get; private set;}


	public virtual void Start () 
	{
		facingRight = true;

		MyAnimator = GetComponent<Animator> ();
	}

	void Update () {
	
	}

	public void ChangeDirection()
	{
		facingRight = !facingRight;
		transform.localScale = new Vector3 (transform.localScale.x * -1, 1, 1);
	}
}
