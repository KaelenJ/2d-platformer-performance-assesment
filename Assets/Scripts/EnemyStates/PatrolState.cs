﻿using UnityEngine;
using System.Collections;

public class PatrolState : IEnemyState
{
	private float patrolTimer;
	private float patrolDuration = 10; 
	private Enemy enemy;

	public void Execute ()
	{
		patrol ();

		enemy.Move ();
	}

	public void Enter (Enemy enemy)
	{
		this.enemy = enemy;
	}

	public void Exit ()
	{

	}

	public void OnTriggerEnter (Collider2D other)
	{

	}

	private void patrol()
	{
		
		patrolTimer += Time.deltaTime;
		
		if(patrolTimer >= patrolDuration)
		{
			enemy.ChangeState(new IdleState());
		}
	}

}
