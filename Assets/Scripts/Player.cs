﻿using UnityEngine;
using System.Collections;

public class Player : Character {

	private static Player instance;

	public static Player Instance
	{
		get 
		{
			if(instance == null)
			{
				instance = GameObject.FindObjectOfType<Player>();
			}
			return instance;
		}
	}


	[SerializeField]
	private Transform[] groundPoints;

	[SerializeField]
	private float groundRadius;

	[SerializeField]
	private LayerMask whatIsGround;

	[SerializeField]
	private float jumpForce;
	
	[SerializeField]
	private bool airControl;

	public Rigidbody2D MyRigibody { get; set; }

	public bool Jump { get; set; }
	public bool OnGround { get; set; }


	public override void Start () 
	{
		MyRigibody = GetComponent<Rigidbody2D> ();
		base.Start ();
	}

	void Update()
	{
		HandleInput ();
	}
	
	// Update is called once per frame
	void FixedUpdate () 
	{
		float horizontal = Input.GetAxis("Horizontal");

		OnGround = IsGrounded ();

		HandleMovement (horizontal);

		Flip (horizontal);
	
		HandleLayers ();
		
	}

	private void HandleMovement(float horizontal)
	{
		if (MyRigibody.velocity.y < 0) 
		{
			MyAnimator.SetBool("land", true);
		}
		if(!Attack && (OnGround || airControl))
		{
			MyRigibody.velocity = new Vector2(horizontal * MovementSpeed,MyRigibody.velocity.y);
		}
		if(Jump && MyRigibody.velocity.y == 0)
		{
			MyRigibody.AddForce(new Vector2(0,jumpForce));
		}

		MyAnimator.SetFloat ("speed", Mathf.Abs (horizontal));
	}


	private void HandleInput()
	{
		if(Input.GetKeyDown(KeyCode.Space))
		{
			MyAnimator.SetTrigger("jump");
		}

		if (Input.GetKeyDown (KeyCode.LeftShift)) 
		{
			MyAnimator.SetTrigger("attack");
		}

	}

	private void Flip(float horizontal)
	{
		if (horizontal > 0 && !facingRight || horizontal < 0 && facingRight) 
		{
			ChangeDirection();
		}

	}

	private bool IsGrounded()
	{
		if (MyRigibody.velocity.y <= 0)
		{
			foreach(Transform point in groundPoints)
			{
				Collider2D[] colliders = Physics2D.OverlapCircleAll(point.position,groundRadius, whatIsGround);

				for(int i = 0; i < colliders.Length; i++)
				{
					if(colliders[i].gameObject != gameObject)
					{
						return true;
					}
				}
			}
		}
		return false;
	}

	private void HandleLayers()
	{
		if (!OnGround) {
			MyAnimator.SetLayerWeight (1, 1);
		} else 
		{
			MyAnimator.SetLayerWeight(1,0);
		}
	}
}
