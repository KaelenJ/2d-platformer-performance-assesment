﻿using UnityEngine;
using System.Collections;

public class Enemy : Character {

	private IEnemyState curretState;

	// Use this for initialization
	public	 override void Start () 
	{
		base.Start ();

		ChangeState (new IdleState ());
	}
	
	// Update is called once per frame
	void Update () 
	{
		curretState.Execute ();	
	}

	public void ChangeState(IEnemyState newState) 
	{
		if(curretState != null)
		{
			curretState.Exit();
		}

		curretState = newState;

		curretState.Enter (this);
	}

	public void Move()
	{
		MyAnimator.SetFloat ("speed", 1);

		transform.Translate (GetDirection () * (MovementSpeed * Time.deltaTime));
	}

			public Vector2 GetDirection()
			{
			return facingRight ? Vector2.right : Vector2.left;
			}
}
